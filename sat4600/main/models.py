from django.db import models
from django import forms

# Create your models here.

class EmployeeInformation(models.Model):
    employeenumber = models.IntegerField()
    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200)
    extension = models.IntegerField(default=0)
    email = models.EmailField(max_length=254)
    jobtitle = models.CharField(max_length=256)
    def __str__(self):
        return self.lastname + ", " + self.firstname

class ProductInformation(models.Model):
    productname = models.CharField(max_length=200)
    productline = models.CharField(max_length=200)
    description = models.TextField()
    price = models.DecimalField(max_digits=256,decimal_places=2)
    def __str__(self):
        return self.productname

class EmployeeIdForm(forms.Form):
    employee_id = forms.CharField(label='Employee ID',max_length=200)

def automcomplete(request):
    search_qs = YourObject.objects.filter(title__icontains=request.REQUEST['search'])[:5]
    results = []
    for r in search_qs:
        results.append(r.title)
    resp = request.REQUEST['callback'] + '(' +simplejson.dumps(results) + ');'
    return HttpResponse(resp,content_type='application/json')
