from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from main.models import EmployeeIdForm

# Create your views here.
def index(request):
    template = loader.get_template('main/index.html')
    return render(request, 'main/index.html')

def news(request):
    return render(request, 'main/news.html')

def products(request):
    return render(request, 'main/products.html')

def reports(request):
    return render(request, 'main/reports.html')

def employee(request):
    if request.method == 'POST':
        form = EmployeeIdForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('/main/')

    else:
        form = EmployeeIdForm()

    return render(request, 'main/employee.html', {'form': form})

def webapp(request):
    return render(request, 'main/webapp.html')
