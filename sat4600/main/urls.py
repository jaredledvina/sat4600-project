from django.conf.urls import patterns, url

from main import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^news/$', views.news, name='news'),
    url(r'^products/$', views.products, name='products'),
    url(r'^employee/$', views.employee, name='employee'),
    url(r'^webapp/$', views.webapp, name='webapp'),
    url(r'^reports/$', views.reports, name='report'),
)
