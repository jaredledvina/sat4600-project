from django.contrib import admin
from main.models import EmployeeInformation,ProductInformation

class EmployeeInformationAdmin(admin.ModelAdmin):
    fields = ['employeenumber', 'firstname', 'lastname', 'extension', 'email', 'jobtitle']
    list_display = ('lastname', 'firstname', 'employeenumber')

class ProductInformationAdmin(admin.ModelAdmin):
    fields = ['productname', 'productline', 'description', 'price']
    list_display = ('productname', 'productline', 'price')

# Register your models here.
admin.site.register(EmployeeInformation, EmployeeInformationAdmin)
admin.site.register(ProductInformation, ProductInformationAdmin)

